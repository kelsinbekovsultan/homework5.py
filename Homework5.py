# Создайте массив состоящий из словарей с данными
abc = [ {
    'dict': 10,
    'dict2': 'bbbb'
},
{
    'dict': 1234,
    'dict2': 4321
},
    {
        'dict': 100,
        'dict2': 12,
    },
    {
        'dict': 123,
        'dict2': 'dddd'
    }
]
# Напишите функцию которая принимает ранее созданный массив, фильтрирует
# полученный массив и возвращающий не менне двух элементов из массива
def filter(arr):
    mass = []
    for i in arr:
        if i['dict'] > 100:
            mass.append(i)

    return mass

filteredmass = filter(abc)

# Напишите функцию которая принимает отфильтрованные данные, добавляет
# новое значение каждому из элементов отфильтрованных данных и возвращает
# измененные данные с добавленными значениями
def newkey(arr):
    for k in arr:
        k['access'] = True
    return arr

addedmass = newkey(filteredmass)
# Напишите функцию которая принимает массив ранее измененых данных,
# меняет значение в каждом из элементов и возращает измененные данные
def change(arr):
    for i in arr:
        i['access'] = False
    return arr

changedmass = change(addedmass)

# Напишите функцию которая принимает массив ранее измененых данных,
# и поочередно выводит их в консоль
def Result(arr):
    for i in arr:
        print(i)

Result(changedmass)
